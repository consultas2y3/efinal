<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
     public function actionConsulta1() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT c.nombre,c.dorsal 
    FROM ciclista c 
    WHERE c.dorsal 
    IN( SELECT e.dorsal 
    FROM etapa e 
    WHERE e.dorsal 
    NOT IN(SELECT dorsal 
    FROM lleva 
    WHERE código=
    (SELECT m.código FROM maillot m WHERE m.color="rosa")))',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre','dorsal'],
                    "titulo" => "Consulta 2",
                    "enunciado" => "la consulta mumero 2 del examen",
                    "sql" => "Lo primero que realizo es sacar el codigo que corresponde a maillot de color rosa, una vez tengo eso, seleccionamos todos los cilistas que han llevado "
            . "ese maillot alguna vez."
            . "luego selecciono los ciclistas que no estan en esa lista, y por ultimo, saco su nombre para mostrarlos."
        ]);
    }
    public function actionConsulta2(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Etapa::find()
                ->select("SUM(kms) AS kilometros"),
                
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['kilometros'],
                    "titulo" => "Consulta 3 con Active Record",
                    "enunciado" => "consulta3",
                    "sql" => 'hacemos un sumatorio de todos los kms de la tabla etapa.'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
